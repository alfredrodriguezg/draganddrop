
import AppRouter from './routes';
import Application from './application'
import ContextProvider from './useContext';
import Header from './Header';
import App2 from './Pages/All';
import Floor from './Pages/Floor';
import Muuri from 'muuri'

function App() {

  
     
  return (
  <>
  <ContextProvider>
    <Header/>
   <Application/>
  </ContextProvider>

  </>
  );
}

export default App;
