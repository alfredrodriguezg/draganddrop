import {useContext } from "react";
import axios from 'axios';
import { dataContext } from "../useContext";
import _ from "lodash";
import { Responsive, WidthProvider } from "react-grid-layout";

const ResponsiveGridLayout = WidthProvider(Responsive);


  const Application = () => {
  const {state,setState}= useContext(dataContext)
   
  const update = async () => {
    const response = await axios.put(
      "http://localhost:3002/v1/articles/collections/order",
      state.app
    );
   
    return response;
  };
  const orden=state.app?.sort((a,b)=>a.applicationsLocations[0].application.order > b.applicationsLocations[0].application.order?1:-1 )
console.log(orden)
  return (
    <>
      <button onClick={update}>send</button>
      <ResponsiveGridLayout
        className="layout"
        onDragStop={(event)=>{
            console.log(event)
        }}
       
        breakpoints={{ lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0 }}
        cols={{ lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }}
    
        rowHeight={250}
        width={1200}
      >
        {
            state.app?.map((collection,i)=>(
                <div key={collection._id} 
                data-grid={{
                    x: i*2%12,
                    y: 1,
                    w: 2,
                    h:  2,
                   
                  }}
                >
                    { i}
                    <div className="card">
                            <img
                              src={collection.applicationsLocations[0].image}
                              style={{ width: "250px" }}
                            />

                            <h4>{collection.subFamily.description}</h4>
  
                          {collection.applicationsLocations.map((item,i)=>(
                            <span key={i} className="location-description">{item.location.description}</span>
                          ))}
                          </div>
                </div>
            ))
        }
      </ResponsiveGridLayout>
     </>
  );
};

export default Application;
