import {useContext } from "react";
import axios from 'axios';
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { dataContext } from "./useContext";


const reOrder=(list,start,end)=>{
    const result =[...list]
    const [removed]=result.splice(start,1)
    result.splice(end,0,removed)
    if(result.length > 60){
      result.findIndex((item,index)=>{
        item.family.order=index
      })
    }else{

      result.findIndex((item,index)=>{
        item.applicationsLocations[0].application.order=index
      })
    }
    return result
  }
  const Application = () => {
  const {state,setState}= useContext(dataContext)
   
  const update = async () => {
    const response = await axios.put(
      "http://localhost:3002/v1/articles/collections/order",
      state.app
    );
   
    return response;
  };
  const orden=state.app?.sort((a,b)=>a.applicationsLocations[0].application.order > b.applicationsLocations[0].application.order?1:-1 )
console.log(orden)
  return (
    <>
      <button onClick={update}>send</button>
      <div style={{ margin: "4em" }}>
        <DragDropContext
          onDragEnd={(result) => {
            const { source, destination } = result;

            console.log(result);
            const order = reOrder(state.app, source.index, destination.index);
           
            setState({...state,app:order})
          }}
        >
          <Droppable droppableId="items">
            {(dropable) => (
              <div
                {...dropable.droppableProps}
                ref={dropable.innerRef}
                style={{ borderBottom: "1px solid red", marginTop: "1em" }}
              >
                {state.app?.map((element, index) => (
                  <Draggable
                    key={element._id}
                    draggableId={element.subFamily.description}
                    index={index}
                  >
                    {(dragable) => (
                      <div
                        {...dragable.draggableProps}
                        ref={dragable.innerRef}
                        {...dragable.dragHandleProps}
                      >
                        {index}
                        {(index<=76)&&
                        
                        <div className="card">
                            <img
                              src={element.applicationsLocations[0].image}
                              style={{ width: "250px" }}
                            />

                            <h4>{element.subFamily.description}</h4>
  
                          {element.applicationsLocations.map((item,i)=>(
                            <span key={i} className="location-description">{item.location.description}</span>
                          ))}
                          </div>
                        
                        }
                        { (index>=76)&&element.applicationsLocations.map((application, i) => (
                          <div key={i} className="card">
                            <img
                              src={application.image}
                              style={{ width: "250px" }}
                            />

                            <h4>{element.subFamily.description}</h4>
  
                          
                          </div>
                        ))}
                       
                      </div>
                    )}
                  </Draggable>
                ))}
                {dropable.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </div>
    </>
  );
};

export default Application;
