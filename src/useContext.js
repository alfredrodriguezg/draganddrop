import { createContext, useEffect, useState } from "react"
import axios from 'axios';


export const dataContext = createContext()
const initialState={
    list:[],
    app:[]
}

const ContextProvider=({children})=>{
const [state,setState]=useState(initialState)

const applicationFilter=(filterName)=>{
  const data=  state.list.map((el) => {
        const { applicationsLocations, ...rest } = el;
        const res = {
          ...rest,
          applicationsLocations: applicationsLocations?.filter(
            (item) => item.application.description === filterName
          ),
        };
        return res;
      })
      .filter((ele) => ele.applicationsLocations[0]);
      if(filterName==="all"){
        setState({...state,app:state.list})
      }else{

          setState({...state,app:data})
      }
}

const locationFilter=(filterName)=>{
    const data=  state.list.map((el) => {
        const { applicationsLocations, ...rest } = el;
        const res = {
          ...rest,
          applicationsLocations: applicationsLocations?.filter(
            (item) => item.location.description === filterName
          ),
        };
        return res;
      })
      .filter((ele) => ele.applicationsLocations[0]);
}
const getData=async()=>{
    const data = await axios.get('http://localhost:3002/v1/articles/collections/order?catalogId=16&lang=es&images=true')

    setState({list:data.data})
}
useEffect(()=>{
    getData()
    
},[])
return(
    <dataContext.Provider value={{state,setState,applicationFilter}}>
        {children}
    </dataContext.Provider>
)
}

export default ContextProvider