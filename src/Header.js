import { useContext } from "react"
import { dataContext } from "./useContext"

const Header = ()=>{

    const{state,applicationFilter}=useContext(dataContext)
    const menu = state.list?.reduce((byApplicationId, collection) => {
        return collection.applicationsLocations.reduce((obj, applicationLocation) => {
          const applicationId = applicationLocation.application.id
    
          return {
            ...obj,
            [applicationId]: {
              ...applicationLocation.application,
          //   location:applicationLocation.location.description,
              subFamilyIds: obj[applicationId]
                ? [...obj[applicationId].subFamilyIds, collection.subFamily.id]
                : [collection.subFamily.id]
            }
          }
        }, byApplicationId)
      }, {})

      console.log(menu)
    return(
<header>
    <div>
      <ul className='header-menu-ul'>
        <li
         onClick={()=>applicationFilter('all')}
        >Todos</li>
        {
         Object.values(menu)?.map((lista)=>(
            <li key={lista.id}
            onClick={()=>applicationFilter(lista.description)}
            >{lista.description}</li>
          ))
        }
      </ul>
      <ul className='header-menu-ul'>
      {
         Object.values(menu)?.map((lista)=>(
            <li key={lista.id}
            onClick={()=>applicationFilter()}
            >{lista.location}</li>
          ))
        }
      </ul>
    </div>
  </header>

    )
}

export default Header